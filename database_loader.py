import json
import os
try:
	# open database file
	with open('database.json', 'r') as json_database:
		database = json.load(json_database)
	print("\nJSON DATABASE HAS BEEN DOWNLOADED AND IS ACCESSED UNDER \"database\" NAME")
	print("ALL PYTHON COMMANDS ARE ALLOWED HERE")
	print("\nYOU CAN EXIT THIS DATABASE EDITOR BY COMMAND \"exit()\" OR CLOSING WINDOW")
	print("\nALL CHANGES MADE TO DATABASE WILL NOT OVERWRITE ORIGINAL DATABASE")
except Exception as e:
	# if database file could not be found
	print("\nDATABASE COULD NOT BE DOWNLOADED\n" + str(e))
	print("\nEDITOR WILL EXIT NOW")
	os._exit(0)
