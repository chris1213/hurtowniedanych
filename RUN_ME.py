from selenium import webdriver
from bs4 import BeautifulSoup
import time
import os
import glob
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import json

#const
SCRIPT_DIRECTORY = os.path.dirname(__file__) #<-- absolute dir the script is in

#globals
job_offers = {}

def scroll_func(driver):
    '''
    Summary:
        Function for scrolling webpage to the end
    Arguments:
        driver (Object 'selenium.webdriver.firefox.webdriver.WebDriver'): Driver responsible for web browsing
    Return:
        result (int): Result of function work
    '''

    SCROLL_PAUSE_TIME = 0.5
    result = 1
    # get scroll height
    last_height = driver.execute_script("return document.body.scrollHeight")

    while True:
        # scroll down to bottom
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

        # wait to load page
        time.sleep(SCROLL_PAUSE_TIME)

        # calculate new scroll height
        new_height = driver.execute_script("return document.body.scrollHeight")

        # if end of page has been reached, stop loop
        if new_height == last_height:
            break
        last_height = new_height
    result = 0
    return result


### EXTRACT PROCESS

def extract():
    '''
    Summary:
        Function for handling extract process
    Arguments:
        None
    Return:
        E_flag (int): Status of extract process
    '''
    print("EXTRACT PROCESS HAS BEEN STARTED\nPLEASE WAIT...")


    # set firefox settings
    firefox_profile = webdriver.FirefoxProfile()
    firefox_profile.set_preference('permissions.default.image', 2)
    firefox_profile.set_preference('dom.ipc.plugins.enabled.libflashplayer.so', 'false')
    caps = DesiredCapabilities().FIREFOX
    caps['marionette'] = True
    # wait for DOMContentLoaded event
    caps["pageLoadStrategy"] = "eager"  # interactive

    # set absolute path to gecko driver
    abs_gecko_path = os.path.join(SCRIPT_DIRECTORY, "geckodriver.exe")

    # run driver
    driver = webdriver.Firefox(firefox_profile = firefox_profile, desired_capabilities = caps, executable_path = abs_gecko_path, service_log_path = 'NUL')

    url = r'https://www.pracuj.pl/praca/krakow%3Bwp'
    page_number = 1
    print("PLEASE WAIT FOR ALL PAGES TO DOWNLOAD...")

    while url:
        try:
            # open url
            driver.get(url)

            # scroll page down
            result = scroll_func(driver)
            if result == 0:
                # do nothing
                pass
            else:
                print("WEBPAGE CANNOT BE SCROLLED")
            # get page source
            soup = BeautifulSoup(driver.page_source, 'html.parser')

            # save page source in html file
            filename = "page_number_%d.html" % page_number
            rel_path = "saved_pages/" + filename
            abs_file_path = os.path.join(SCRIPT_DIRECTORY, rel_path)
            with open(abs_file_path, 'wb') as file:
                file.write(soup.encode('utf-8'))

            # localize the next page button
            next_button = soup.select('.pagination_element--next')[0].select('a')[0]['href']

            # set url to the next page
            url = 'https://www.pracuj.pl' + next_button
            print(f"PAGE: {url}")
            page_number += 1
        except Exception:

            # all pages has been downloaded
            break
    # close browser
    driver.close()

    # print result
    print(str(page_number) + " WEBPAGES HAS BEEN SAVED TO FILE")
    print("EXTRACT PROCESS HAS ENDED SUCCESSFULLY")

    # set extraction status to "done"
    E_flag = 1
    return E_flag


### TRANSFORM PROCESS

def transform(E_flag):
    '''
    Summary:
        Function for handling transform process

    Arguments:
        E_flag (int): Status of extract process

    Return:
        T_flag (int): Status of transform process
    '''
    global job_offers

    print("TRANSFORM PROCESS HAS BEEN STARTED\nPLEASE WAIT...")

    # check if extract process has been performed
    if E_flag == 1:
        soup = None
        rel_path = "saved_pages/"
        abs_path = os.path.join(SCRIPT_DIRECTORY, rel_path)

        # for all pages in /saved_pages directory
        for file_name in os.listdir(abs_path):

            # get file name and absolute directory
            rel_path = "saved_pages/" + file_name
            abs_file_path = os.path.join(SCRIPT_DIRECTORY, rel_path)

            # load saved page source
            with open(abs_file_path, 'rb') as file:
                soup = BeautifulSoup(file.read().decode('utf-8'), features="lxml")

            # for each '.offer__info' tag which is unique for every job offer
            for i in soup.select('.offer__info'):

                # get job name
                try:
                    job_name = i.select('.offer-details__title-link')[0].text
                except:
                    job_name = " "

                # get compane web page
                try:
                    company_webpage = i.select('.offer-company__link')[0]['href']
                except:
                    company_webpage = " "

                # get job description
                try:
                    description = i.select('.offer-description__content')[0].text.replace('\n','')
                except:
                    description = " "

                # get job offer link
                try:
                    job_offer_webpage = r'https://www.pracuj.pl' + i.select('.offer-details__title-link')[0]['href']
                except:
                    job_offer_webpage = " "

                # get company name
                try:
                    company_name = i.select('.offer-company__name')[0].text
                except:
                    company_name = " "

                # get publication date
                publication_date = " "
                try:
                    # publication date is not within offer_info, but in parent element
                    for element in i.parent.select('.offer-actions__date'):
                        publication_date = element.text

                    # modify retrieved text
                    publication_date = publication_date.replace("opublikowana: ", " ")
                    publication_date = publication_date.replace("\n", " ")
                except:
                    publication_date = " "

                # get additional content for job offer
                additional_content = i.select(".offer-labels__item")
                location = " "
                salary = " "
                position = " "
                working_time = " "
                contract_type = " "
                additional_info = " "

                # additional content fields are not required and because of it,
                # order of its items is dynamic. Searching is based on icons sticked to field name
                for item in additional_content:
                    if 'mdi-map-marker' in str(item):
                        location = item.text
                    if 'mdi-circle-slice-5' in str(item):
                        working_time = item.text
                    if 'mdi-signal' in str(item):
                        position = item.text
                    if 'mdi-file-document' in str(item):
                        contract_type = item.text
                    if 'mdi-file-compare' in str(item):
                        additional_info = item.text
                    if 'mdi-database' in str(item):
                        salary = item.text

                # job offers' names are unique for every company
                primary_key = job_name + " - " + company_name
                job_offers[primary_key] = {
                    "job_name": job_name,
                    "publication_date": publication_date,
                    "company_webpage": company_webpage,
                    "description": description,
                    "job_offer_webpage": job_offer_webpage,
                    "company_name": company_name,
                    "location": location,
                    "working_time": working_time,
                    "position": position,
                    "contract_type": contract_type,
                    "salary": salary,
                    "additional_info": additional_info
                    #"wymagania": wymagania
                }

                print("RECORDS TRANSFORMED: " + str(len(job_offers.keys())))

           # print tranform result
        print(str(len(job_offers.keys())) + " JOB OFFERS HAS BEEN FOUND")
        print("TRANSFORM PROCESS HAS ENDED SUCCESSFULLY")

        # set transform status to "done"
        T_flag = 1

    else:
        print("PLEASE EXECUTE EXTRACT PROCESS BEFORE TRANSFORM")
        T_flag = 0

    return T_flag


### LOADING PROCESS

def load(T_flag, actualize):
    '''
    Summary:
    Function responsible for handling loading process

    Arguments:
    T_flag (int): Status of transform process
    actualize (int): Do or do not overwrite existing records in database

    Return:
    E_flag (int): Status of extract process
    T_flat (int): Status of transform process
    '''
    global job_offers

    print("LOAD PROCESS HAS BEEN STARTED\nPLEASE WAIT...")

    # get database absolute path
    database_filename = "database.json"
    database_abs_path = os.path.join(SCRIPT_DIRECTORY, database_filename)

    # check if transform process has been performed
    if T_flag == 1:

        # check if database exists
        if os.path.exists(database_abs_path):

            # if database exists set opening type to read
            mode = 'r'
        else:

            # if database does not exists set opening type to editable and/or create
            mode = 'w+'

        # open database file
        with open(database_abs_path, mode) as json_database:
            try:
                saved_database = json.load(json_database)
            except Exception as e:
                print(str(e) + "\nSAVED DATABASE IS EMPTY OR CORRUPTED, NO RECORDS HAS BEEN DOWNLOADED")
                saved_database = {}

        # if actualize flag is set to 0, do not overwrite existing records
        if actualize == 0:
            # for every record in saved database, delete records in extracted and transormed database
            for job_offer_key in saved_database:
                if job_offer_key in job_offers:
                    del job_offers[job_offer_key]

            # get new records number
            new_job_offers = len(job_offers.keys())
        else:
            print("DATABASE WILL BE UPDATED")

        # update database with new records
        saved_database.update(job_offers)

        # save new database
        with open(database_abs_path, 'w', encoding = 'utf-8') as database_file:
            json.dump(saved_database, database_file)

        # print load results
        if actualize == 0:
            print(str(new_job_offers) + " NEW JOB OFFER(S) HAS BEEN ADDED")
        else:
            print("DATABASE HAS BEEN UPDATED")

        print("FILES CREATED IN EXTRACT PROCESS WILL NOW BE DELETED\nPLEASE WAIT...")
        rel_path = "saved_pages\\*"
        abs_path = os.path.join(SCRIPT_DIRECTORY, rel_path)
        files = glob.glob(abs_path)
        for file in files:
            os.remove(file)
            print("FILE " + file.replace(SCRIPT_DIRECTORY, "") + " HAS BEEN REMOVED")

        # clear local database
        job_offers = {}
        print("LOAD PROCESS HAS ENDED SUCCESSFULLY")
    else:
        print("PLEASE EXECUTE TRANSFORM PROCESS BEFORE LOAD")

    # set statuses of extract and transform processes as "need to be done"
    E_flag = 0
    T_flag = 0
    return [E_flag, T_flag]

'''
'''
def save_as_CSV():
    '''
    Summary:
        Function for saving database into a CSV file
    Arguments:
        None
    Return:
        result (int): Result of function work
    '''
    # get database absolute path
    database_filename = "database.json"
    database_abs_path = os.path.join(SCRIPT_DIRECTORY, database_filename)

    # download database
    with open(database_abs_path, 'r', encoding = 'utf-8') as json_database:
        database = json.load(json_database)

    # get csv absolute path
    csv_filename = "csv_file.csv"
    csv_abs_path = os.path.join(SCRIPT_DIRECTORY, csv_filename)

    # create csv file
    with open(csv_abs_path, 'w+', encoding = 'utf-8') as csv_file:

        # create first row of columt names
        one_offer_primary_key = list(database.keys())[0]
        for key in database[one_offer_primary_key]:
            csv_file.write(key)

            # set delimiter to (@)
            csv_file.write("@")

        # go to new line
        csv_file.write('\n')

        # for every field, in every record, save it in csv file, with (@) as delimiter
        for job_offer_key in database:
            for key in database[job_offer_key]:
                csv_file.write(database[job_offer_key][key])
                csv_file.write('@')

            csv_file.write('\n')

    # calculate database records
    records = len(database.keys())

    # print saving CSV results
    print("CSV file has been created with " + str(records) + " RECORDS")
    result = 0
    return result

def delete_database():
    '''
    Summary:
        Function for deleting records in the database
    Arguments:
        None
    Return:
        result (int): Result of function work
    '''
    rel_path = "database.json"
    abs_path = os.path.join(SCRIPT_DIRECTORY, rel_path)
    with open(abs_path, 'w', encoding = 'utf-8') as database_file:
        database_file.write("")
    result = 0
    return result


def draw_main_menu_options():
    '''
    Summary:
        Function for drawing menu options
    Arguments:
        None
    Return:
        result (int): Result of function work
    '''
    print("[E] Extract process")
    print("[T] Transform process")
    print("[L] Load process")
    print("[ETL] Whole ETL process")
    print("[CSV] Save database as CSV file")
    print("[ACT] Actualize database")
    print("[DEL] Delete whole database")
    print("[ANL] Analyze database")
    print("[EXT] Exit program")
    return 0


def main_menu_loop():
    '''
    Summary:
        Function responsible for handling user commands
    Arguments:
        None
    Result:
        result (int): Result of function work
    '''
    E_flag = 0
    T_flag = 0
    # clear command line
    os.system('cls')
    option = ""
    while option != "EXT":
        result = draw_main_menu_options()
        if result == 0:
            # do nothing
            pass
        else:
            print("AN ERROR OCCURED DURING DRAWING MAIN MENU")
        # retrive chosing option from user, and capitalize all letters
        option = input(">> ").upper()

        # based on selected option, perform action
        if option == "E":
            try:
                E_flag = extract()
            except Exception as e:
                print("EXTRACTION PROCESS FAILED\n" + str(e))
        elif option == "T":
            try:
                T_flag = transform(E_flag)
            except Exception as e:
                print("TRANSFORM PROCESS FAILED\n" + str(e))
        elif option == "L":
            try:
                E_Flag, T_flag = load(T_flag, 0)
            except Exception as e:
                print("LOAD PROCESS FAILED\n" + str(e))
        elif option == "ETL":
            try:
                E_flag = extract()
            except Exception as e:
                print("EXTRACTION PROCESS FAILED\n" + str(e))
            try:
                T_flag = transform(E_flag)
            except Exception as e:
                print("TRANSFORM PROCESS FAILED\n" + str(e))
            try:
                E_flag, T_flag = load(T_flag, 0)
            except Exception as e:
                print("LOAD PROCESS FAILED\n" + str(e))
        elif option == "CSV":
            try:
                result = save_as_CSV()
                if result == 0:
                    print("SAVING TO CSV SUCCESSFUL")
                else:
                    # wrong result handled in exception
                    pass
            except Exception as e:
               print("SAVING TO CSV FAILED\n" + str(e))
        elif option == "ACT":
            try:
                E_flag = extract()
            except Exception as e:
                print("EXTRACTION PROCESS FAILED\n" + str(e))
            try:
                T_flag = transform(E_flag)
            except Exception as e:
                print("TRANSFORM PROCESS FAILED\n" + str(e))
            try:
                load(T_flag, 1)
            except Exception as e:
                print("LOAD PROCESS FAILED\n" + str(e))
        elif option == "DEL":
            try:
                result = delete_database()
                if result == 0:
                    print("DATABASE DELETION SUCCESSFUL")
                else:
                    # wrong result handled by exception
                    pass
            except Exception as e:
                print("DELETING DATABASE PROCESS FAILED\n" + str(e))
        elif option == "ANL":
            try:
                # new python file will be runned by "viewer.bat" file
                filename = "viewer.bat"
                abs_path = os.path.join(SCRIPT_DIRECTORY, filename)

                # if path folders contains white characters, path needs to be surrounded with quotes
                abs_path = "\"" + abs_path + "\""

                # execute file
                os.system(abs_path)
            except Exception as e:
                print("ANALYZER PROCESS CANNOT BE STARTED\n" + str(e))
        elif option == "EXT":
            print("PROGRAM WILL FINISH AFTER KEY IS PRESSED")
        else:
            print("THERE IS NO SUCH OPTION AS " + option)

        # wait for key press
        os.system('pause')

        # clear command line
        os.system('cls')
    return 1

#set browser running type to headless (not visible)
os.environ['MOZ_HEADLESS'] = '1'

main_menu_loop()

#set browser running type to normal
os.environ['MOZ_HEADLESS'] = '0'